import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import wordcount.{FastCharacterReaderImpl, SlowCharacterReaderImpl}

import scala.concurrent.Await
import scala.concurrent.duration.Duration


object App {
  def main(args: Array[String]) {
    print("Hello count word, we will be running the first part of our processing!!!")

    //1st part
    /** Implement a function that takes a CharacterReader and produces a list of words, in order of
descending frequency. When two words have the same frequency, they should be alphabetical order.*/
    val wordProvider = new FastCharacterReaderImpl()

    val simpleFastReader = new WordCount {}
    val doWhenFinished = () => {}
    val simpleWords = simpleFastReader.countWords(0, wordProvider,doWhenFinished) //0 means read all words rather than none
    simpleWords.foreach(x=>println(s"${x.name}-${x.frequency}"))

    println("GOING TO THE SECOND PART")
    /**
      * 2nd part
      * You are given 10 instances of the above interface, which sleep for a random amount of time before
returning a character. Write code that will read these in parallel. It should output the current
(combined) counts of the words every 10 seconds and final totals at the end.
      */

    val sources = (1 to 10).map(_=>new SlowCharacterReaderImpl()).toIterator
    val actorSystem = ActorSystem("reading")
    val manager = actorSystem.actorOf(WordManager.props(printDelay = 10000,noOfReaderActors = 10)(actorSystem),"actor-manager")
    manager ! StartComputation(sources)

    Await.result(actorSystem.whenTerminated, Duration(1, TimeUnit.MINUTES))

  }


}
