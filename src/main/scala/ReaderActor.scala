import java.util.concurrent.TimeUnit
import java.util.{Timer, TimerTask}

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, PoisonPill, Props}
import wordcount.CharacterReader
import akka.util.Timeout

import scala.concurrent.{Future, Promise}
import scala.util.Try
case class StartComputation(sources: Iterator[CharacterReader])
case class PrintInformation(words: List[Word])
case class StartReading(source: CharacterReader)
case class WordsRead(words: List[Word])
case object FinishedReading
case class FinishPrinting(words: List[Word])
case object FinishedPrinting

object WordManager{
  def props(printDelay: Int, noOfReaderActors: Int)(implicit system: ActorSystem) : Props = Props(new WordManager(printDelay,noOfReaderActors))
}
class WordManager(printDelay: Int, noOfReaderActors: Int)(implicit system:ActorSystem) extends Actor with ActorLogging{
  val readingKids = scala.collection.mutable.ListBuffer.empty[ActorRef]
  val printerKid = system.actorOf(PrinterActor.props(getATerminal()))
  var words = List.empty[Word]


  val future = delay[Unit](printDelay)(sendPrintCommand())

  override def preStart(): Unit = {
    (1 to noOfReaderActors).foreach(_=>{
      readingKids.append(system.actorOf(ReaderActor.props(self)))
    })
  }

  def getATerminal() = {
    new Terminal[String]{
      override def write(t: String): String = {
        print(t)
        t
      }
    }
  }
  override def receive: Receive = {
    case StartComputation(sources)=>{
      readingKids.foreach(actor=> actor ! StartReading(sources.next()))
    }
    case WordsRead(wordsRecv) =>
      words = mergeListOfWords(words,wordsRecv)

    case p@PrintInformation => printerKid.forward(p)
    case FinishedReading =>
      readingKids.--=(List(sender))
      readingKids.isEmpty match{
        case true =>
          implicit val timeout = Timeout(15, TimeUnit.SECONDS)
          printerKid ! FinishPrinting(words)
        case _ =>
      }
    case FinishedPrinting => context stop self

    case x => log.info(s"[WordManager]got some unknown message ${x.toString}")

  }


  override def postStop(): Unit = {
    printerKid ! PrintInformation(List.empty) //before i leave they should print it once more
    readingKids.foreach(actor=>actor ! PoisonPill)
    printerKid ! PoisonPill

    future._1.cancel()
    self ! PoisonPill
    system.terminate()
  }

  def sendPrintCommand()={
    printerKid ! new PrintInformation(words)
  }

  private def delay[T](delay: Long)(block: => T): (Timer,Future[T]) = {
    val promise = Promise[T]()
    val t = new Timer()
    t.schedule(new TimerTask {
      override def run(): Unit = {
        promise.tryComplete(Try(block))
      }
    },0, delay)

    (t,promise.future)
  }

  private def mergeListOfWords(a: List[Word], b:List[Word]): List[Word] = {
    val all = (a ::: b).groupBy(_.name).map{
      case (name: String,words: List[Word])=>
        Word(name,words.foldLeft(0){
          (a:Int,w:Word)=> a+ w.frequency
        })
    }.toList.sortBy(_.name).sortBy(_.frequency)

    all
  }
}


object ReaderActor{
  def props(parent: ActorRef): Props =
    Props(new ReaderActor(parent))
}
class ReaderActor(parent: ActorRef) extends Actor with WordCount with ActorLogging {
  var stop = false
  override def receive: Receive = {
    case StartReading(source) =>
      stop match{
        case true => parent ! FinishedReading
        case false => self ! StartReading(source)
      }

      val words = countWords(10,source, stopMe)
      parent ! WordsRead(words)

    case FinishedReading => {
      stop = true
    }
    case unknownMessage => log.info(s"[ReaderActor]got an unknown message: ${unknownMessage.toString}")
  }

  private def stopMe():Unit ={
    self ! FinishedReading
  }
}

trait Terminal[C]{
  def write(t: C): C
}

object PrinterActor{
  def props(terminal: Terminal[String]): Props =
    Props(new PrinterActor(terminal))
}
class PrinterActor(terminal: Terminal[String]) extends Actor with ActorLogging{

  override def receive: Receive = {
    case PrintInformation(wordsRecv) =>
      printWords(wordsRecv)

    case FinishPrinting(wds) =>
      printWords(wds)
      sender() ! FinishedPrinting
      self ! PoisonPill
  }

  private def printWords(words: List[Word])={
    terminal.write("\n")
    words.foreach(w =>{
      terminal.write(s"${w.name}: ${w.frequency}\t")
    })
  }
}