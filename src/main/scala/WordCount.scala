import java.io.EOFException
import wordcount.CharacterReader
import scala.annotation.tailrec


case class Word(name: String, frequency: Int)

trait WordCount {
  val alphabet = ('a' to 'z')


  def countWords(n:Int, reader: CharacterReader,doWhenFinished: ()=>Unit):List[Word] = {
    countWordsAndSort(getListOfWords(n,reader, doWhenFinished))
  }

  def countWordsAndSort(words: List[String]): List[Word] = {
    words.foldLeft(Map.empty[String, Int]) {
      (count, word) => count + (word -> (count.getOrElse(word, 0) + 1))
    }.map{
      case(str:String,freq:Int) => Word(str,freq)
    }.toList.sortBy(_.name).sortBy(_.frequency)
  }

  def isInAlphabet(char: Char): Boolean = alphabet.contains(char.toLower)

  def readWord(reader: CharacterReader, doWhenFinished: ()=>Unit = ()=>{}): (Option[String],Boolean)  = {
    @tailrec
    def readWordFromStream(currWord: String): (Option[String],Boolean) = {
      readCharCheckEOF(reader, doWhenFinished ) match{
        case None => (Some(currWord),true)
        case Some(cw) if(!isInAlphabet(cw))=>  (Some(currWord),false)
        case Some(cw)  => readWordFromStream(currWord+cw)
      }
    }
    readWordFromStream("")
  }

  def readCharCheckEOF(reader: CharacterReader, doWhenFinished: ()=>Unit): Option[Char] = {
    try{
      Option(reader.nextCharacter()).map(_.toLower)
    } catch{
      case ex: EOFException =>
        doWhenFinished()
        None
    }
  }

  def getListOfWords(n:Int = 0, reader: CharacterReader, doWhenFinished: ()=>Unit):List[String] = {

    def getColWords(noW:Int, currList: List[String]) :List[String] = noW match {
      case nw: Int if (nw<=n || n==0) =>  readWord(reader,doWhenFinished) match {
        case (Some(wd),true) => wd :: currList
        case (Some(wd),false) => getColWords(nw+1, wd :: currList)
      }
      case _=> currList
    }


    getColWords(0,List.empty[String]).filterNot(_.isEmpty).reverse
  }
}
