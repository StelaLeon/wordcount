import akka.actor.ActorSystem
import org.scalatest.Matchers
import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActors, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import wordcount.{FastCharacterReaderImpl, SlowCharacterReaderImpl}

class ReaderTestSpec extends TestKit(ActorSystem("MySpec"))
  with ImplicitSender
with WordSpecLike with Matchers with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "An Reader actor" must {
    "send back read messages unchanged" in {
      val echoParent = TestProbe()
      val reader = system.actorOf(ReaderActor.props(echoParent.ref))
      reader ! StartReading(new FastCharacterReaderImpl("hello world"))
      val expectedWords = WordsRead(List(Word("hello",1),Word("world",1)))
      echoParent.expectMsg(expectedWords)
    }
  }
}
